# VNS
**Made by Xzing**

**A basic frame for visual novels**

# How to use
## General
VNS is ordered with scene. 
Each scene is has a background and up to 4 characters. A scene may end with a player choice.
## How to make scenes
Scenes are created with godot.
In order to make a scene, you need to modify the following elements :

- Scene name, which is an id
- Each of the 4 characters sprites a sprite has two aspects : on and of. Sprites which are unused much be turned unvisible.
- The background image
## How to use txt files
Each line is an instuction. A line is ended with a ;
The first character determines the type of action that will be executes :

- "[" changes the frame of a character
- "-" is for text lines
- ">" is for choices
- "<" is for consequences of a choice
- "@" switches scene (see godot scene for more)

If you put a "$" in the text, it will be replaced by character's name.