extends Node

# warning-ignore:unused_class_variable
func decrypt(content):
	var blockType:String
	var blockContent:String

	for contentChar in content:
		if(contentChar != "\n"): #ignore the \n in .res files
			if(contentChar == ";"): #; is the end block char : push and reset
				globals.partition.push_back([blockType, blockContent])
				blockType = "";
				blockContent = "";
				
			elif(blockType == ""): #first char of each block determines it's type
				blockType = contentChar
				
			else: #then it's the content
				match(contentChar): #some special chars will execute actions
					"~": blockContent += "\n"
					"$": blockContent += globals.mainCharName
					_:   blockContent += contentChar
