extends Node2D

var cursorPos:int = 0
var nbPlacedElements:int = 0

func _ready():
	load_new_scene()
	forward_action()



func load_new_scene():
	var temp_file = File.new()
	temp_file.open("res://txtdata/" + String(globals.sceneId) + ".res", File.READ)
	var temp_content = temp_file.get_as_text()
	temp_file.close()
	reader.decrypt(temp_content)

func forward_action():
	var type = globals.partition[0][0]
	var content = globals.partition[0][1]
	#the func call will be recursive if there's no stop, which is why we need to remove before calling execute_action()
	globals.partition.remove(0)
	execute_action(type, content)



func execute_action(type:String, content:String):
	match type:
		"-": $Text/Text.text = content
		"[": change_char_sprite(content)
		">": offer_choice(content)
		"<": forward_scene(content)
		"#": place_element(content)
		"@": change_to_special_scene(content)
		_: forward_action() #skip if the char is not one detected



func change_char_sprite(content:String):
# warning-ignore:integer_division
# warning-ignore:integer_division
	$Chars.get_child(int(content.left(1))).frame = int(content.right(1))
	forward_action()


func offer_choice(content:String):
	$Choice.visible = true
	$Choice/Text.text += content + "\n"
	forward_action()


func forward_scene(sceneId:String):
	if(!cursorPos): #if cursor is at pos 0, pick choice 0
		globals.partition.clear() #clear the part
# warning-ignore:return_value_discarded
		globals.sceneId = int(sceneId)
# warning-ignore:return_value_discarded
		get_tree().change_scene("res://scenes/BasicScene.tscn")
		load_new_scene()
	else:
		cursorPos -= 1
		forward_action() #else decrease cursor and call this func again


func place_element(content:String):
	if (content == "vide"):
		$Chars.get_child(nbPlacedElements-1).visible = false
	elif(nbPlacedElements == 0):
		$Background.texture = load("res://imgs/"+content+".png")
	else:
		$Chars.get_child(nbPlacedElements-1).texture = load("res://imgs/"+content+".png")
	nbPlacedElements+=1
	if(nbPlacedElements == 5):
		nbPlacedElements = 0
	forward_action()


func change_to_special_scene(content:String):
	get_tree().change_scene("res://scenes/"+content+".tscn")



# warning-ignore:unused_argument
func _physics_process(delta):
	if(Input.is_action_just_pressed("ui_accept")):
		forward_action()

	if(Input.is_action_just_pressed("ui_down")):
		if(!cursorPos && $Choice.visible):
			cursorPos = 1
			$Choice/Cursor.position.y += 24

	if(Input.is_action_just_pressed("ui_up")):
		if(cursorPos && $Choice.visible):
			cursorPos = 0
			$Choice/Cursor.position.y -= 24
